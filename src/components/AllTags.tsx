import Jobs from "./Jobs";

import React, { useState } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";

import { Accordion, Button, Card, Table } from "react-bootstrap";

const TAGS_QUERY = gql`
  query {
    allTags {
      tagName
      tagDesc
      imgSrc
    }
  }
`;
const AllTags = () => {
  const { loading, error, data } = useQuery(TAGS_QUERY, {});

  const [tagName, setTagName] = useState("");
  const [selectedTags, setSelectedTags] = useState([]);
  return (
    <>
      {loading && <h1>Loading</h1>}
      {error && <h1>Error</h1>}
      <Table>
        <tr>
          {data &&
            data.allTags.map(function (tag, index) {
              return (
                <td>
                  <input
                    type="checkbox"
                    onChange={(e) => {
                      if (e.target.checked) {
                        setTagName(tag.tagName);
                        setSelectedTags(selectedTags.concat(tag.tagName));
                      } else {
                        setSelectedTags(
                          selectedTags.filter((item) => item !== tag.tagName)
                        );
                        if (selectedTags.length === 0) {
                          setTagName("");
                        } else {
                          setTagName(selectedTags[0]);
                        }
                      }
                    }}
                    id={tag.tagName}
                  />
                  <label htmlFor={tag.tagName}>
                    {" "}
                    <img src={tag.imgSrc} />{" "}
                  </label>
                </td>
              );
            })}
        </tr>
      </Table>
      
      <Table>
        {selectedTags.length != 0 &&
          selectedTags.map(function (tag, index) {
            return (
              <tr>
                <td> {tag}</td>
                <td>
                  {" "}
                  <Jobs name={tag} />{" "}
                </td>
              </tr>
            );
          })}
      </Table>
    </>
  );
};

export default AllTags;