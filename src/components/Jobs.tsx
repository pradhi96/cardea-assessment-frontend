import React, { useState } from "react";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";

import Table from "react-bootstrap/Table";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const JOBS_QUERY = gql`
  query JobQuery($tagName: String!) {
    jobPostingByTag(name: $tagName) {
      jobTitle
      companyName
      companyLocation
      minSalary
      maxSalary
      applyLink
    }
  }
`;

const Jobs = (props) => {
  const tagName = useState(props.name);
  console.log("type");
  console.log(typeof props.name);
  const { loading, error, data } = useQuery(JOBS_QUERY, {
    variables: { tagName: props.name },
  });

  return (
    <>
      {data && (
        <Table>
          <thead>
            <tr>
              <th> Title </th>
              <th> Company </th>
              <th> Location</th>
            </tr>
          </thead>

          {data.jobPostingByTag.map(function (posting, index) {
            return (
              <tr>
                {" "}
                <td> {posting.jobTitle} </td> <td> {posting.companyName} </td>
                <td>{posting.companyLocation} </td>
                <td>
                  <Accordion>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <Typography>Show more</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Typography>
                        <span>  Min Salary {posting.minSalary}</span>
                        <br />
                        <span>  Max Salary {posting.maxSalary}</span>
                        <br />
                         <a href = {posting.applyLink}>Apply </a>
                      </Typography>
                    </AccordionDetails>
                  </Accordion>
                </td>
              </tr>
            );
          })}
        </Table>
      )}
    </>
  );
};

export default Jobs;
