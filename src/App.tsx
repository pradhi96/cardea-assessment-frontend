import React from 'react';
import HelloWorld from "./components/HelloWorld";
import AllTags from "./components/AllTags";

function App() {
  return (
    <div className="App">
      <AllTags/>
    </div>
  );
}

export default App;
